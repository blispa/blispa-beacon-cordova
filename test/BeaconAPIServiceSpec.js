'use strict';

describe('BlispaAPIService', function() {
  //beforeEach(module('blispa-directory'));
  var promiseResolution;

  /**
  Accepts a function that returns a promise, and if it's an Ajax request,*/

  /**
   * Must do the promise in the beforeEach() function, and call done() when the promise resolves
   * so that we can do expects on the returned value in the it() tests.
   * This is a helper function to avoid copying it into each test.
   * @param promiseProducer - A function that returns a promise to be started and waited for until it returns.
   * @param mockAjaxResolution - If the promise does an Ajax request, you can mock the value that will be returned here.
   */
  var beforeEachResolvePromise = function(promiseProducer, mockAjaxResolution){
    beforeEach( function(done){
      //Must set up the promise in the beforeEach function, so that APIResponse has been populated
      // by the time we reach the actual test.
      promiseProducer()
        .then( function(response){
          promiseResolution = response;
          done();
        }, function(err){
          fail('promise was rejected:'+err);
        });

      if( mockAjaxResolution ) {
        //Mock the request to get.
        var request = jasmine.Ajax.requests.mostRecent();
        request.respondWith(mockAjaxResolution);
      }
    });
  };

  //Must send the deviceReady event initially so it starts listening for pause in the browser (on a phone, cordova does this).
  var event = new CustomEvent("deviceready", {});
  document.dispatchEvent(event);

  beforeEach( function(){
    jasmine.Ajax.install();
    localStorage.clear();
    BeaconAPIService.initialize();

    //TODO: Reset the singleton BlispaAPIService's state.
  });

  afterEach(function(){
    jasmine.Ajax.uninstall();
    BeaconAPIService.cachedBeacons = {};

    localStorage.clear();
  });

  describe('get / set options', function(){
    xit('Uses the set ApiKey in a request', function(){
      BeaconAPIService.setReadToken("TestToken");
      BeaconAPIService.getBeaconInfo("000/0/0", false, "ProximityNear", "ProximityFar");
      var request = jasmine.Ajax.requests.mostRecent();
      expect( request.requestHeaders.Authorization).toContain("TestToken");
    });
  });

  describe('GET beacon success functionality', function(){
    beforeEachResolvePromise(
      function(){ return BeaconAPIService.getBeaconInfo("02424c49-5350-4f00-9dbf-3f5307b1159a/16/101", false, "ProximityNear", "ProximityFar");
      },
      mockResponses.socialKingpin.success
    );
    it('gets the details for a beacon', function(){
      expect(_.isEmpty(promiseResolution)).toBe(false);
      expect(promiseResolution.name).toBe('BathEscapes Game');
    });

    it('stores the beacon details for later retrieval via getAllCachedBeacons', function(){
      expect(_.size(BeaconAPIService.getAllCachedBeacons())).toBe(1);
    });

    describe('does not use the cache when skipHttpIfCached set to false', function(){
     beforeEachResolvePromise(function() {
         return BeaconAPIService.getBeaconInfo("02424c49-5350-4f00-9dbf-3f5307b1159a/16/101", false, "ProximityNear", "ProximityFar");
       },
       mockResponses.socialKingpin.updated );
      it('responds with updated data', function(){
        expect(_.isEmpty(promiseResolution)).toBe(false);
        expect(promiseResolution.name).toBe('BathEscapes Game UPDATED');
      });
    });

    describe('uses the cache on 2nd request when skipHttpIfCached = true', function(){
      beforeEachResolvePromise(function() {
          return BeaconAPIService.getBeaconInfo("02424c49-5350-4f00-9dbf-3f5307b1159a/16/101", true, "ProximityNear", "ProximityFar");
        },
        mockResponses.socialKingpin.updated );
      it('responds with the OLD, cached data', function(){
        expect(_.isEmpty(promiseResolution)).toBe(false);
        expect(promiseResolution.name).toBe('BathEscapes Game');
      });
    });

    describe('Can retrieve an action from a previously loaded beacon', function(){
      beforeEachResolvePromise(function() {
          return BeaconAPIService.getAction(70);
        });

      it('returns an action', function(){
        expect(_.isEmpty(promiseResolution)).toBe(false);
        expect(promiseResolution.answers[0]).toBe('Nash');
      });
    });

  });

  describe('Get beacon failure: 404', function () {
    beforeEach(function (done) {
      BeaconAPIService.getBeaconInfo("000/1/2").finally(function () {
        done();
      });
      var request = jasmine.Ajax.requests.mostRecent();
      request.respondWith({status: 404});
    });

    it('Keeps errored beacon in the beacon Cache', function () {
      expect(_.size(BeaconAPIService.cachedBeacons)).toBe(1);
      expect(BeaconAPIService.cachedBeacons["000/1/2"].error).toBe(404);
    });

    it('Does not return the errored beacon from getAllCachedBeacons', function () {
      expect(_.size(BeaconAPIService.getAllCachedBeacons())).toBe(0);
    });

    describe('Second attempt after 404', function(){
      var ajaxRequestsBeforeDoingSecondRequest;
      var promiseResult = "none";
      beforeEach(function (done) {
        ajaxRequestsBeforeDoingSecondRequest = jasmine.Ajax.requests.count();
        BeaconAPIService.getBeaconInfo("000/1/2", true).then(
          function (response) {
            promiseResult = "resolved";
            promiseResolution = response;
            done();
          }, function(err){
            promiseResult = "rejected";
            promiseResolution = err;
            done();
          });
      });

      it('does not do another ajax request after erroring', function () {
        expect( jasmine.Ajax.requests.count() ).toBe( ajaxRequestsBeforeDoingSecondRequest );
      });

      it('rejects the promise returned after the error happens', function () {
        expect(promiseResult).toBe("rejected");
      });
    });
  });



  describe('Refresh beacon cache', function(){
    beforeEachResolvePromise(function(){
        return BeaconAPIService.refreshBeaconCache();
      },
      mockResponses.threeGameBeacons.success);

    it('Adds all beacons to the cache.', function(){
      expect(_.size(BeaconAPIService.getAllCachedBeacons())).toBe(3);
    });

    it('Returns cached beacon data, keyed by beaconID from getAllCachedBeacons()', function(){
      expect(BeaconAPIService.getAllCachedBeacons()["02424c49-5350-4f00-9dbf-3f5307b1159a/16/100"].latitude).toBe('51.383307');
    });

    describe('After refreshing beacon cache', function(){
      beforeEachResolvePromise(function(){
        return BeaconAPIService.getBeaconInfo("02424c49-5350-4f00-9dbf-3f5307b1159a/16/100", true);
      });

      it('Retrieves a beacon from the cache on subsequent getBeacon calls', function(){
        expect(promiseResolution.name).toBe('BathEscapes Game');
      });
    });

  });
  
  describe('store events and send later', function() {
    var ajaxRequestsCountAtStart;
    beforeEach(function () {
      //Reset state between tests.
      BeaconAPIService.logEvents.length = 0;
      ajaxRequestsCountAtStart = jasmine.Ajax.requests.count();
    });

    it('Does not do an ajax request if no events are pending', function () {
      expect( jasmine.Ajax.requests.count() ).toBe( ajaxRequestsCountAtStart );
    });

    describe('with 2 logs to send', function() {
      beforeEach(function () {
        //Add some events.
        BeaconAPIService.recordEvent({name: "Some Event"}, false);
        BeaconAPIService.recordEvent({name: "Other Event"}, false);
      });

      it('Stores events internally', function () {
        expect(_.size(BeaconAPIService.logEvents)).toBe(2);
      });

      //TODO: Loads existing events from persistent storage when created. Difficult to test due to Singleton.

      describe('sendLoggedEvents', function () {
        beforeEach(function () {
          ajaxRequestsCountAtStart = jasmine.Ajax.requests.count();
          BeaconAPIService.sendLoggedEvents();
        });

        it('Makes an AJAX request if there are events to send', function(){
          expect( jasmine.Ajax.requests.count() ).toBe( ajaxRequestsCountAtStart + 1 );
        });

        it('Sets DeviceTime Correctly', function () {
          var ajaxRequestMade = jasmine.Ajax.requests.mostRecent();
          var paramsSent = JSON.parse(ajaxRequestMade.params);
          expect(paramsSent.deviceTime).toBeCloseTo(Date.now() / 1000, -1);
        });

        it('Sends events in payload', function () {
          var ajaxRequestMade = jasmine.Ajax.requests.mostRecent();
          var paramsSent = JSON.parse(ajaxRequestMade.params);
          expect(paramsSent.events.length).toBe(2);
          /* jshint camelcase:false */
          expect(paramsSent.events[0].occurred_at).toBeCloseTo(Date.now() / 1000, -1);
          expect(paramsSent.events[0].event_data.name).toBe("Some Event");
        });
      });

      describe('sendLoggedEvents AJAX success', function () {
        beforeEach(function (done) {
          BeaconAPIService.sendLoggedEvents().finally(function () {
            done();
          });
          var request = jasmine.Ajax.requests.mostRecent();
          request.respondWith({status: 200, responseText: "[]"});
        });


        it('Clears logged events from memory if AJAX request succeeds', function () {
          expect(BeaconAPIService.logEvents.length).toBe(0);
        });
        it('Clears logged events from persistent storage if AJAX request succeeds', function () {
          expect(window.localStorage.getItem('logEvents')).toBe(null);
        });
      });

      describe('sendLoggedEvents AJAX failure', function () {
        beforeEach(function (done) {
          BeaconAPIService.sendLoggedEvents().finally(function () {
            done();
          });
          var request = jasmine.Ajax.requests.mostRecent();
          request.respondWith({status: 500});
        });

        it('Keeps logged events in memory', function () {
          expect(BeaconAPIService.logEvents.length).toBe(2);
        });

        it('Saves logged events to persistent storage', function () {
          var storedEvents = JSON.parse(window.localStorage.getItem('logEvents'));
          expect(storedEvents.length).toBe(2);
        });
      });

      it('sends Logged Events on pause event', function () {
        spyOn(BeaconAPIService, 'sendLoggedEvents');
        var event = new CustomEvent('pause', {});
        document.dispatchEvent(event);

        expect(BeaconAPIService.sendLoggedEvents).toHaveBeenCalled();
      });

      it('sends Logged Events after a timeout.', function (done) {
        spyOn(BeaconAPIService, 'sendLoggedEvents');
        BeaconAPIService.setLoggedEventsInterval(10);

        setTimeout(function () {
          expect(BeaconAPIService.sendLoggedEvents).toHaveBeenCalled();
          done(); //done() prevents test succeeding immediately when the timeout has been set but not yet run.
        }, 100);
      });

      it('Does NOT send Logged Events before the timeout.', function (done) {
        spyOn(BeaconAPIService, 'sendLoggedEvents');
        BeaconAPIService.setLoggedEventsInterval(100);

        setTimeout(function () {
          expect(BeaconAPIService.sendLoggedEvents).not.toHaveBeenCalled();
          done(); //done() prevents test succeeding immediately when the timeout has been set but not yet run.
        }, 10);
      });
    });
  });





  describe('store logs and send later', function() {
    var ajaxRequestsCountAtStart;
    beforeEach(function () {
      //Reset state between tests.
      BeaconAPIService.logGetBeaconInfo.length = 0;
      ajaxRequestsCountAtStart = jasmine.Ajax.requests.count();
    });

    it('Does not do an ajax request if no logs are pending', function () {
      expect( jasmine.Ajax.requests.count() ).toBe( ajaxRequestsCountAtStart );
    });

    describe('with 2 logs to send', function() {
      beforeEach(function () {
        BeaconAPIService.storage.setItem('lastSentLoggedBeacons', new Date().getTime() );
        BeaconAPIService.getBeaconInfo("000/1/100");
        BeaconAPIService.getBeaconInfo("000/1/101");
      });

      it('Stores logs internally', function () {
        expect(_.size(BeaconAPIService.logGetBeaconInfo)).toBe(2);
      });

      describe('sendLoggedBeacons', function () {
        beforeEach(function () {
          ajaxRequestsCountAtStart = jasmine.Ajax.requests.count();
          BeaconAPIService.sendLoggedBeacons();
        });

        it('Does an AJAX request if there are logs to send', function(){
          expect( jasmine.Ajax.requests.count() ).toBe( ajaxRequestsCountAtStart + 1 );
        });

        it('Sets DeviceTime Correctly', function () {
          var ajaxRequestMade = jasmine.Ajax.requests.mostRecent();
          var paramsSent = JSON.parse(ajaxRequestMade.params);
          expect(paramsSent.deviceTime).toBeCloseTo(Date.now() / 1000, -1);
        });

        it('Sends logs in payload', function () {
          var ajaxRequestMade = jasmine.Ajax.requests.mostRecent();
          var paramsSent = JSON.parse(ajaxRequestMade.params);
          expect(paramsSent.logs.length).toBe(2);
          /* jshint camelcase:false */
          expect(paramsSent.logs[0].occurred_at).toBeCloseTo(Date.now() / 1000, -1);
          expect(paramsSent.logs[0].uuid).toBe("000");
          expect(paramsSent.logs[0].major).toBe("1");
          expect(paramsSent.logs[0].minor).toBe("100");
        });

      });


      describe('sendLoggedBeacons AJAX success', function () {
        beforeEach(function (done) {
          BeaconAPIService.sendLoggedBeacons().finally(function () {
            done();
          });
          var request = jasmine.Ajax.requests.mostRecent();
          request.respondWith({status: 200, responseText: "[]"});
        });


        it('Clears logged beacons from memory if AJAX request succeeds', function () {
          expect(BeaconAPIService.logGetBeaconInfo.length).toBe(0);
        });
        it('Clears logged beacons from persistent storage if AJAX request succeeds', function () {
          expect(window.localStorage.getItem('logGetBeaconInfo')).toBe(null);
        });
      });

      describe('sendLoggedBeacons AJAX failure', function () {
        beforeEach(function (done) {
          BeaconAPIService.sendLoggedBeacons().finally(function () {
            done();
          });
          var request = jasmine.Ajax.requests.mostRecent();
          request.respondWith({status: 500});
        });

        it('Keeps logged beacons in memory', function () {
          expect(BeaconAPIService.logGetBeaconInfo.length).toBe(2);
        });

        it('Saves logged beacons to persistent storage', function () {
          var storedLogs = JSON.parse(window.localStorage.getItem('logGetBeaconInfo'));
          expect(storedLogs.length).toBe(2);
        });
      });

      it('sends Logged Beacons on pause event', function () {
        spyOn(BeaconAPIService, 'sendLoggedBeacons');
        var event = new CustomEvent("pause", {});
        document.dispatchEvent(event);

        expect(BeaconAPIService.sendLoggedBeacons).toHaveBeenCalled();
      });

      it('sends Logged Beacons after a timeout.', function (done) {
        spyOn(BeaconAPIService, 'sendLoggedBeacons');
        BeaconAPIService.setLoggedEventsInterval(10);

        setTimeout(function () {
          expect(BeaconAPIService.sendLoggedBeacons).toHaveBeenCalled();
          done(); //done() prlogs test succeeding immediately when the timeout has been set but not yet run.
        }, 100);
      });

      it('Does NOT send Logged Logs before the timeout.', function (done) {
        spyOn(BeaconAPIService, 'sendLoggedBeacons');
        BeaconAPIService.setLoggedEventsInterval(100);

        setTimeout(function () {
          expect(BeaconAPIService.sendLoggedBeacons).not.toHaveBeenCalled();
          done(); //done() prevents test succeeding immediately when the timeout has been set but not yet run.
        }, 10);
      });
    });

  });
});
