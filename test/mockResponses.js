'use strict';

window.mockResponses = {
  socialKingpin: {
    success: {
      status: 200,
      responseText: JSON.stringify({
        "id": "02424c49-5350-4f00-9dbf-3f5307b1159a/16/101",
        "uuid": "02424c49-5350-4f00-9dbf-3f5307b1159a",
        "major": 16,
        "minor": 101,
        "name": "BathEscapes Game",
        "description": "",
        "latitude": "51.382197",
        "longitude": "-2.362459",
        "actions": [
          {
            "type": "Question",
            "app_id": 6,
            "detail": {
              "image": "http://timehunter.blispa.com/bath/SocialKingpin.jpg",
              "thumbnail": "http://timehunter.blispa.com/bath/SocialKingpin_th.jpg",
              "title": "Social Kingpin",
              "body": "Bath's infamous social kingpin occupied this house, he left a lively history. Earning his nickname because of his flamboyant style, his surname is no mystery. What is his surname?",
              "fact": "Richard 'Beau' Nash, notorious dandy and gambler with a bevy of mistresses, arrived in Bath in 1703 as an aide de camp to the then Master of Ceremonies, Captain Webster. The young Richard Nash took his place when the Captain was killed in a duel. He is considered key in bringing the town to social prominence in the Georgian social calendar and a notable spa town revival. The present main entrance to the theatre was built in 1720 as Beau Nash's first house and opened 9 days before the Battle of Trafalgar. It replaced the original theatre in Old Orchard Street and is considered as one of the most important theatres outside of London.  This was fourth since 1705. The original entrance on the north side of the theatre has railings around the lawn which are replicas of seamen's pikes used at the time of Lord Nelson's death and placed there in his memory.",
              "answers": [
                "Nash"
              ],
              "instruction": "Put the sorry story of Beau Nash behind you and walk 100 paces along Upper Borough Walls with Gascoyne Place Restaurant to your left, until you find a wall steeped in history.",
              "letter": 3,
              "id": 70
            }
          }
        ]
      })
    },
    updated: {
      status: 200,
      responseText: JSON.stringify({
        "id": "02424c49-5350-4f00-9dbf-3f5307b1159a/16/101",
        "uuid": "02424c49-5350-4f00-9dbf-3f5307b1159a",
        "major": 16,
        "minor": 101,
        "name": "BathEscapes Game UPDATED",
        "description": "",
        "latitude": "51.382197",
        "longitude": "-2.362459",
        "actions": [
          {
            "type": "Question",
            "app_id": 6,
            "detail": {
              "image": "http://timehunter.blispa.com/bath/SocialKingpin.jpg",
              "thumbnail": "http://timehunter.blispa.com/bath/SocialKingpin_th.jpg",
              "title": "Social Kingpin",
              "body": "Bath's infamous social kingpin occupied this house, he left a lively history. Earning his nickname because of his flamboyant style, his surname is no mystery. What is his surname?",
              "fact": "Richard 'Beau' Nash, notorious dandy and gambler with a bevy of mistresses, arrived in Bath in 1703 as an aide de camp to the then Master of Ceremonies, Captain Webster. The young Richard Nash took his place when the Captain was killed in a duel. He is considered key in bringing the town to social prominence in the Georgian social calendar and a notable spa town revival. The present main entrance to the theatre was built in 1720 as Beau Nash's first house and opened 9 days before the Battle of Trafalgar. It replaced the original theatre in Old Orchard Street and is considered as one of the most important theatres outside of London.  This was fourth since 1705. The original entrance on the north side of the theatre has railings around the lawn which are replicas of seamen's pikes used at the time of Lord Nelson's death and placed there in his memory.",
              "answers": [
                "Nash"
              ],
              "instruction": "Put the sorry story of Beau Nash behind you and walk 100 paces along Upper Borough Walls with Gascoyne Place Restaurant to your left, until you find a wall steeped in history.",
              "letter": 3,
              "id": 70
            }
          }
        ]
      })
    }
  },

  threeGameBeacons: {
    success: {
      status: 200,
      responseText: JSON.stringify(
        [
          {
            "id": "02424c49-5350-4f00-9dbf-3f5307b1159a/16/100",
            "uuid": "02424c49-5350-4f00-9dbf-3f5307b1159a",
            "major": 16,
            "minor": 100,
            "name": "BathEscapes Game",
            "description": "",
            "latitude": "51.383307",
            "longitude": "-2.357153",
            "actions": [
              {
                "type": "Question",
                "app_id": 6,
                "detail": {
                  "image": "http://timehunter.blispa.com/bath/BookstorePaintedWindow.jpg",
                  "thumbnail": "http://timehunter.blispa.com/bath/BookstorePaintedWindow_th.jpg",
                  "title": "Trompe-l'oeil",
                  "body": "Find the man with his nose in a book. The place he is reading is the place you must look.",
                  "fact": "In the original design of Pulteney Street, a column was erected that was modelled on Nelsons Column in Trafalgar Square. However, the locals found this too overbearing and voted to replace it with a water feature (sadly damaged and replaced with the current fountain in 1970).",
                  "answers": [
                    "Library",
                    "Lending Library",
                    "Bookshop",
                    "BookStore",
                    "Book Store"
                  ],
                  "instruction": "Take the left turning at the fountain down Henrietta Street, and into Henrietta Park on your right. Just inside you will find an inner gate to the Remembrance Garden.",
                  "letter": 2,
                  "id": 80
                }
              }
            ]
          },
          {
            "id": "02424c49-5350-4f00-9dbf-3f5307b1159a/16/109",
            "uuid": "02424c49-5350-4f00-9dbf-3f5307b1159a",
            "major": 16,
            "minor": 109,
            "name": "BathEscapes Game",
            "description": "",
            "latitude": "51.380611",
            "longitude": "-2.359069",
            "actions": [
              {
                "type": "Question",
                "app_id": 6,
                "detail": {
                  "image": "http://timehunter.blispa.com/bath/RootsManoeuvre.jpg",
                  "thumbnail": "http://timehunter.blispa.com/bath/RootsManoeuvre_th.jpg",
                  "title": "Roots manoeuvre",
                  "body": "The majestic tree that you will see is a Plane. Planted when is what you must guess an answer so to gain:",
                  "fact": "Abbey Green once boasted a Bishops Palace. A landlord of The Crystal Palace unearthed a roman mosaic and several skeletons during works in 1981 and it remains in place today.",
                  "possible_answers": [
                    {
                      "text": "1690",
                      "html": "1690",
                      "correct": false
                    },
                    {
                      "text": "1790",
                      "html": "1790",
                      "correct": true
                    },
                    {
                      "text": "1890",
                      "html": "1890",
                      "correct": false
                    }
                  ],
                  "answers": [
                    "Seventeen Ninety",
                    "1790"
                  ],
                  "instruction": "Exit the square via North Parade Passage, and stop outside Sally Lunns.",
                  "letter": 1,
                  "id": 77
                }
              }
            ]
          },
          {
            "id": "02424c49-5350-4f00-9dbf-3f5307b1159a/16/112",
            "app_id": 6,
            "uuid": "02424c49-5350-4f00-9dbf-3f5307b1159a",
            "major": 16,
            "minor": 112,
            "name": "BathEscapes Game",
            "description": "",
            "latitude": "51.385627",
            "longitude": "-2.356298",
            "actions": [
              {
                "type": "Question",
                "app_id": 6,
                "detail": {
                  "image": "http://timehunter.blispa.com/bath/RemembranceGarden.jpg",
                  "thumbnail": "http://timehunter.blispa.com/bath/RemembranceGarden_th.jpg",
                  "title": "Remembrance Garden",
                  "body": "Bill and Gwen loved this park, what was their surname?",
                  "fact": "Henrietta Street is part of the 'new town' area of Bath, built in the 1780's when Thomas Baldwin worked with Sir William Pulteney to divide up what had been cow pastures by the riverside. It fast became the most fashionable part of the city. Henrietta Park is a city centre jewel, opened to celebrate the Diamond Jubilee of Queen Victoria in 1897. It was later re-designed as a garden for the blind being especially planted out with scented herbs, shrubs and flowers.",
                  "answers": [
                    "Bascombe"
                  ],
                  "instruction": "Exit the park the way you entered and back along Henrietta Street, back over Pulteney Bridge and past the Victoria Art Gallery adjoining the Guildhall. Continue to the end of the road, cross over and stop opposite Mallory.",
                  "letter": 2,
                  "id": 81
                }
              }
            ]
          }
        ])
    }
  }
};