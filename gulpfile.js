'use strict';
console.time("Loading plugins"); //start measuring
var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
//var minifyCss = require('gulp-minify-css');
//var rename = require('gulp-rename');
//var sh = require('shelljs');
console.timeEnd("Loading plugins"); //end measuring

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('build', function() {
 	var jsFiles = ['src/MockBeaconService.js', 'src/CordovaBeaconService.js', 'src/BeaconAPIService.js', 'src/BlispaBeacon.js'];
 	gulp.src(jsFiles)
		.pipe(concat('blispa-beacon-cordova.js'))
		.pipe(gulp.dest('dist'));
 });

gulp.task('watch', function() {
  gulp.start('build');
  gulp.watch(['src/**'], ['build']);
});