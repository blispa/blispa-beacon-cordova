#!/bin/bash

if git status | grep -q 'On branch master' ; then
  if git status | grep -q 'nothing to commit, working tree clean'; then
    git checkout release && git merge master --no-commit --log && gulp build && git add dist/* && git commit --no-edit && bower version patch && git push && git push --tags && git checkout master && gulp build
  else
    echo "Working directory not clean.";
  fi
else
  echo "Not on master branch."
fi
