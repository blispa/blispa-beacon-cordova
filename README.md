#Blispa-Beacon-Cordova

This library wraps the [cordova-plugin-ibeacon cordova plugin](https://github.com/petermetz/cordova-plugin-ibeacon) in
order to hide a lot of the low level complexity that is not needed for the majority of applications. It handles
ranging beacons constantly when the app is in the foreground, ignoring false-positives and false-negatives (where
a beacon seems to appear or disappear for a single second) and double-detections by applying smoothing over a few
seconds and maintaining an internal list of the currently visible beacons. It also transparently handles looking
up beacons in the Blispa API and emits any actions found related to that beacon as javascript events, and makes a
historical list of actions available to your application. Finally, localStorage is used to return cached data if the
Blispa API can't be reached. 

## Install

### Cordova-Plugin
This library depends on the cordova-plugin-ibeacon cordova plugin
 
    cordova plugin add https://github.com/petermetz/cordova-plugin-ibeacon.git

And the app-preferences plugin (for setting preferences that the background service uses)

    cordova plugin add https://github.com/apla/me.apla.cordova.app-preferences

### Javascript Libraries

If you are using bower, `bower install https://bitbucket.org/blispa/blispa-beacon-cordova.git --save` will install this library along with its dependencies. If you are not using bower, download /dist/main.js and the dependencies (jQuery, lodash, Q.js) into your project's /lib manually.

You can then load the files from your index.html file:
    
    <script src="lib/lodash/lodash.js"></script>
    <script src="lib/q/q.js"></script>
    <script src="lib/jQuery/dist/jquery.js"></script>
    <script src="lib/loglevel/dist/loglevel.js"></script>
    <script src="lib/blispa-beacon-cordova/dist/blispa-beacon-cordova.js"></script>
    
## Usage

Just loading the main.js file will start listening for any Blispa beacons (ie. those with the UUID `02424C49-5350-4F00-9DBF-3F5307B1159A`)
 and will emit 'beaconMoved' events on `document` as beacons come in / go out of range (after waiting a few seconds to remove false move events).
 
~~~~
//This will create a new DIV each time a new beacon moves between ranges (ie. near, far, immediate or out of range).
document.addEventListener('beaconMoved', function(event){
    $('.app').append('<div>Beacon '+event.detail.beacon.id+' '+event.detail.direction+' from '+event.detail.oldProximity+' to '+event.detail.newProximity+'</div>');
}, false);
~~~~

~~~~
//This will check what beacons are currently within the "near" or "immediate" range and show those beacons on screen.
document.addEventListener('beaconMoved', function(event){
    $('.app').empty();
    window.BlispaBeacon.getNearBeacons().forEach( function(beacon){
        console.log(beacon);
        $('.app').append('<div>'+beacon.id+'</div>');
    });
}, false);
~~~~
 
If you have set up beacons in the Blispa API (using your write token to do some PUTs), 'beaconAction' events will also be emitted with your custom JSON object.

~~~~
//This example uses the Blispa REST API to retrieve a custom JSON object with a title and body attribute.
// The object returned will be different for each beacon (in this case, it is for the TimeHunter quiz app, where a different question is shown at each location).
// You will need a beacon configured with the Blispa UUID and majorId=16 (0x0010) and minorId=1,2 or 3 to get a response from the API.
window.BlispaBeacon.setAPIReadToken("cc2dcdd12279595622821be010f43f4f"); //use the readToken issued to your app to see your own data.
document.addEventListener('beaconAction', function(event){
    var action = event.detail.action;
    $('.app').append('<div><B>'+action.title+'</B><BR>'+action.body+'</div>');
});
~~~~

Use BeaconAPIService.setLoggedEventsInterval( ms ) to change the buffer time for which events are cached before they are sent to the server.

When testing in a browser, simulated beacons appear & disappear in turn every ~10 seconds. By default there is a single dummy beacon that appears with UUID="02424C49-5350-4F00-9DBF-3F5307B1159A", Major=16 (0x0010), Minor=1 (0x0001). You can add to these by calling window.BlispaBeacon.addDummyRegion( UUID, major, minor) and each dummy regions will appear in turn every 10 seconds. On Android or iOS, we will use real beacons, and the addDummyRegion function will not do anything.

## Wishlist

 * Background notifications on Android (requires functionality to be added to the cordova plugin and AltBeacon libraries first, see pending issues: [https://github.com/petermetz/cordova-plugin-ibeacon/issues/141](https://github.com/petermetz/cordova-plugin-ibeacon/issues/141) and [https://github.com/AltBeacon/android-beacon-library/issues/151](https://github.com/AltBeacon/android-beacon-library/issues/151)).
 * Keep track of beacons within a configurable distance. Currently BlispaBeacon.getNearBeacons() returns only those beacons with ProximityNear or ProximityImmediate.
 * Metrics based on API calls - how much your beacons are being triggered.
 * Remove some javascript dependencies.
 * Tests