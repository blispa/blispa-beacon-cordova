'use strict';
(function(){

  var log = window.log.getLogger('BlispaBeacon');

  function getUniqueActionsWithAppIds(actions){
    var uniquedActions = {};
    //If there are 2 identical actions from different apps, condense them into a single action with both app IDs in the app_id property.
    actions.forEach( function(appAction){
      var actionDetail = appAction.detail;
      actionDetail.type = appAction.type; //Merge type onto the detail.

      //If action on beacon already exists (from a previous app), add to its actions.
      if(uniquedActions[actionDetail.id]) {
        /* jshint camelcase:false */
        uniquedActions[actionDetail.id].app_ids.push(appAction.app_id);
      }
      else{ //Create the Action.
        actionDetail.app_ids = [appAction.app_id]; //Because we emit "actions", the receiving code might need to know what app it came from, so put app_id on the action.
        uniquedActions[actionDetail.id] = actionDetail;
      }
    });
    return _.values(uniquedActions);
  }

  var cacheDeferred = Q.defer(); //It is best not to rely on cacheDeferred resolving for normal operation, as it might never resolve if there is no internet.
  var BeaconAPIService = {
    blispaURL: "https://beacons.blispa.com/api",
    cdnUrl: "https://cdn.blispa.com/api",
    readToken: "",
    logEvents: [],
    logGetBeaconInfo: [],
    cachedBeacons: {}, //We maintain both an in-memory cache (for quick, non-promisey lookups) and a persistent cache that lasts between app restarts and is accessible from the java worker.
    beaconsLoggedRecently: {},
    sendEventsInterval: null,
    sendEventsIntervalTime: null,

    getAllCachedBeacons: function(){ //Should this be public? TODO: Look where it's used and possibly replace with getCachedBeaconCount.
      return _.pick(BeaconAPIService.cachedBeacons, function(val, key){ return !val.error; });
    },

    getCachedBeaconCount: function(){
      return _.size(BeaconAPIService.getAllCachedBeacons());
    },

    setReadToken: function( token ){
      BeaconAPIService.readToken = token;

      //Send to the backend Service so we can continue to use the API key when running in the background.
      document.addEventListener('deviceready', function() {
        if (window.plugins && window.plugins.appPreferences) {
          window.plugins.appPreferences.store('APIReadToken', token);
        }
      });
    },

    setAPIURL: function( url ){
      BeaconAPIService.blispaURL = url;

      //Send to the backend Service so we can continue to use the API key when running in the background.
      document.addEventListener('deviceready', function() {
        if (window.plugins && window.plugins.appPreferences) {
          window.plugins.appPreferences.store('APIURL', url);
        }
      });
    },


    setWPAPIURL: function( url ){
      BeaconAPIService.wpURL = url;

      //Send to the backend Service so we can continue to use the API key when running in the background.
      document.addEventListener('deviceready', function() {
        if (window.plugins && window.plugins.appPreferences) {
          window.plugins.appPreferences.store('WPAPIURL', url);
        }
      });
    },


    /**
     * Gets the details for a beacon, either from local cache or by making API request.
     * Also records a "seen beacon" log event for this device.
     * Returns a clone of the cached item, so changes made to the returned object will NOT be reflected next time they are retrieved from the cache.
     * @param beaconId String ID in form UUID/MAJOR/MINOR
     * @param skipHttpIfCached Set to true to allow use of the local cache and avoid going over the network (usually, you will want true). Defaults to false.
     * @param oldProximity
     * @param newProximity
     * @returns A promise that will be fulfilled with the beacon's details.
     */
    getBeaconInfo: function(beaconId, skipHttpIfCached, oldProximity, newProximity){
      /* jshint camelcase:false */
      /*if(BeaconAPIService.logGetBeaconInfo.length < 10000 ) {
        BeaconAPIService.logGetBeaconInfo.push({
          uuid: beaconId.split("/")[0],
          major: beaconId.split("/")[1],
          minor: beaconId.split("/")[2],
          old_proximity: oldProximity,
          new_proximity: newProximity,
          occurred_at: Math.floor(new Date().getTime() / 1000)
        });
      }*/
      //Send logged beacons if we haven't sent them in a while (this happens if the interval failed to fire- can happen on iOS if app has been in background).
      BeaconAPIService.storage.getItem('lastSentLoggedBeacons').then(function(lastSentLoggedBeacons){
        if( new Date().getTime() - lastSentLoggedBeacons > BeaconAPIService.sendEventsIntervalTime ){
          log.info('sending loggedBeacons as its been '+((new Date().getTime() - lastSentLoggedBeacons)/1000 )+' since we last sent them.');
          BeaconAPIService.sendLoggedBeacons();
          BeaconAPIService.sendLoggedEvents();
        }
        else{
          log.info('not sending loggedEvents as last sent them '+ ((new Date().getTime() - lastSentLoggedBeacons)/1000 )+' seconds ago.');
        }
      });
      if( skipHttpIfCached ){
        if( BeaconAPIService.cachedBeacons[beaconId.toLowerCase()] && !BeaconAPIService.cachedBeacons[beaconId.toLowerCase()].error) {
          return Q.when(_.cloneDeep(BeaconAPIService.cachedBeacons[beaconId.toLowerCase()]));
        }
        //Don't recheck if we already checked in the last hour.
        else if( BeaconAPIService.cachedBeacons[beaconId.toLowerCase()] && BeaconAPIService.cachedBeacons[beaconId.toLowerCase()].lastUpdated > new Date().getTime() - 60*60*1000 ) {
          return Q.reject("404 from cache");
        }
      }
      return Q(jQuery.ajax({
      method: 'GET',
      url: BeaconAPIService.blispaURL+"/beacons/"+beaconId.toLowerCase()+"?deep=true",
      timeout: 3000, //Should be cached anyway, so abort quickly if we can't get through.
      headers: {
        'Authorization': 'Token '+BeaconAPIService.readToken,
        'OldProximity': oldProximity,
        'NewProximity': newProximity,
        'DeviceID': (window.device? window.device.uuid : "Browser")
        }
      })).then( function(beaconResponse){
        beaconResponse.id = (beaconResponse.uuid+"/"+beaconResponse.major+"/"+beaconResponse.minor).toLowerCase();
        //Put actions into the format we expect, with app_ids: [...]
        beaconResponse.actions = getUniqueActionsWithAppIds(beaconResponse.actions);
        //Update the cache, so the new beacon is stored (possibly with the new, uncached, actions that we're about to emit)
        BeaconAPIService.storage.setItem(beaconResponse.id, beaconResponse);
        BeaconAPIService.cachedBeacons[beaconId.toLowerCase()] = beaconResponse;
        BeaconAPIService.storage.setItem('cachedBeacons', BeaconAPIService.cachedBeacons);
        BeaconAPIService.refreshBeaconCacheIfNeeded();
        return _.cloneDeep(beaconResponse);
      },function(err){
        //Retrieve from cached storage.
        if( BeaconAPIService.cachedBeacons[beaconId.toLowerCase()] && !BeaconAPIService.cachedBeacons[beaconId.toLowerCase()].error ){
          return _.cloneDeep(BeaconAPIService.cachedBeacons[beaconId.toLowerCase()]);
        }
        else{
          //If the server responded, but it was a 404, and we have nothing cached already, then store the error for a little while so we don't ping API every time we see the beacon.
          if(err.status === 404){
            BeaconAPIService.cachedBeacons[beaconId.toLowerCase()] = {error: 404, lastUpdated: new Date().getTime()};
          }
          return Q.reject(err);
        }
      });
    },

    addToBatchLogs: function(beacon){
      /* jshint camelcase:false */
      if(BeaconAPIService.logGetBeaconInfo.length < 10000 ) {
        //Do not log if we already saw this beacon within the last 60 secs.
        if( !BeaconAPIService.beaconsLoggedRecently[beacon.id] || BeaconAPIService.beaconsLoggedRecently[beacon.id] < new Date().getTime() - 60*1000 ) {
          BeaconAPIService.logGetBeaconInfo.push({
            uuid: beacon.uuid,
            major: beacon.major,
            minor: beacon.minor,
            rx: beacon.rssi,
            tx: beacon.tx,
            accuracy: beacon.accuracy,
            occurred_at: Math.floor(new Date().getTime() / 1000)
          });
          BeaconAPIService.beaconsLoggedRecently[beacon.id] = new Date().getTime();
        }
      }
    },

    refreshBeaconCache: function(){
      //Cache from API
      return Q(jQuery.ajax({
        method: 'GET',
        url: (BeaconAPIService.cdnUrl ? BeaconAPIService.cdnUrl : BeaconAPIService.blispaURL)+"/beacons?deep=true",
        timeout: 20000,
        headers: {
          'Authorization': 'Token '+BeaconAPIService.readToken,
          'Accept': 'application/json'
        }
      }))
      .then( function(response){
        //Re-key by beacon-ID for easy lookups that we're going to want to do most of the time.
        response.forEach( function(beacon){
          beacon.actions = getUniqueActionsWithAppIds(beacon.actions);
          BeaconAPIService.cachedBeacons[beacon.id] = beacon;
        });
        log.info('Setting cachedBeacons to:');
        log.info(BeaconAPIService.cachedBeacons);
        return BeaconAPIService.storage.setItem('cachedBeacons', BeaconAPIService.cachedBeacons)
        .then( function() {
          return BeaconAPIService.storage.setItem('lastUpdatedBeaconCache', new Date().getTime());
        })
        .then(function(){
          return cacheDeferred.resolve();
        });
      }, function(err){
        console.error('could not refresh beacon cache');
        console.error(err);
      });
    },

    refreshBeaconCacheIfNeeded: function(){
      return BeaconAPIService.storage.getItem('lastUpdatedBeaconCache')
      .then( function(lastUpdated){
        if( !lastUpdated || (new Date().getTime() - lastUpdated) > 4*60*60*1000){
          log.info('Refreshing Beacon Cache');
          return BeaconAPIService.refreshBeaconCache();
        }
        else{
          log.info('not refreshing beacon cache as it is recent');
          return cacheDeferred.resolve();
        }
      });
    },

    getAction: function(actionId){
      if( typeof actionId !== "number" || actionId % 1 !== 0){
        console.error('BlispaBeacon.getAction was passed an actionId that was not an integer.');
      }
      var ret;
      //TODO: Query the API for this action first (requires API to implement /actions/X endpoint).
      //Find the action with this ID inside the storage. Optimization: Build a hash keyed by action ID.
      _.values(BeaconAPIService.getAllCachedBeacons()).forEach( function(beaconData) {
        if (beaconData.actions && Array.isArray(beaconData.actions)) {
          beaconData.actions.forEach(function (action) {
            if (action.id === actionId) {
              ret = _.clone(action);
            }
          });
        }
      });
      return Q.when(ret); //We return a promise for consistency with and for future-compatibility with an /actions/x API endpoint.
    },

    getApps: function(){
      return Q(jQuery.ajax({
        method: 'GET',
        url: BeaconAPIService.blispaURL+"/apps",
        timeout: 20000,
        headers: {
          'Authorization': 'Token '+BeaconAPIService.readToken,
          'Accept': 'application/json'
        }
      }));
    },

    /**
     * Returns all actions of a particular type (eg. all question, or all advertisements.
     * @param actionType String name of the type of action, or "all".
     * @returns {{}} a map of action id => action. eg. {1: {id: 1, type: question, title: "What is 1+2?"}, 2: {id: 2, type: question, title: "What is the opposite of North?"} }.
     */
    getCachedActionsOfType: function(actionType){
      var cachedActions = {};
      _.values(BeaconAPIService.cachedBeacons).forEach( function(beacon) {
        if( beacon.actions && Array.isArray( beacon.actions ) ) {
          beacon.actions.forEach(function (action) {
            if (action.type === actionType || actionType === 'all') {
              cachedActions[action.id] = _.clone(action);
            }
          });
        }
      });
      return cachedActions;
    },

    /**
     * Creates or updates a trackable device. Links the given name / email / description with the device so it appears
     * in the beacons dashboard and permission can be given for users to track this device.
     * @param deviceName - A friendly name for this device.
     * @param deviceDescription - Optional description for additional data on device.
     * @returns {*} the Q-wrapped HTTP promise for the PUT call.
     */
    registerDevice: function(deviceName, deviceDescription){
      var device = {
        name: deviceName
      };
      if( deviceDescription ){
        device.description = deviceDescription;
      }

      return Q(jQuery.ajax({
        method: 'PUT',
        url: BeaconAPIService.blispaURL + "/trackables/devices/"+(window.device? window.device.uuid : "Browser"),
        headers: {
          'Authorization': 'Token ' + BeaconAPIService.readToken,
        },
        data: {
          device: device
        }
      }));
    },

    recordEvent: function(eventData, sendImmediate){
      if( sendImmediate ){
        return Q(jQuery.ajax({
          method: 'POST',
          url: BeaconAPIService.blispaURL+"/events",
          headers: {
            'Authorization': 'Token '+BeaconAPIService.readToken,
            'DeviceID': (window.device? window.device.uuid : "Browser")
          },
          data: {"event": {"event_data": eventData} }
        }))
        .fail( function(){
          //If we couldn't get through to save the event, save the event for later.
          /* jshint camelcase: false */
          BeaconAPIService.logEvents.push( {event_data: eventData, occurred_at: Math.floor(new Date().getTime()/1000)} );
        });
      }
      else{
        /* jshint camelcase: false */
        BeaconAPIService.logEvents.push( {event_data: eventData, occurred_at: Math.floor(new Date().getTime()/1000)} );
      }
    },

    sendLoggedEvents: function(){
      if( BeaconAPIService.logEvents.length >= 1 ){
        var eventsPayload = {"deviceTime": Math.floor(new Date().getTime()/1000), "events": BeaconAPIService.logEvents };
        BeaconAPIService.logEvents = []; //Create new array so any future events go into a new list to be sent next time.
        log.info( "Logging events..."+ JSON.stringify(eventsPayload) );
        return Q(jQuery.ajax({
          method: 'POST',
          url: BeaconAPIService.blispaURL+"/eventsbatch",
          headers: {
            'Authorization': 'Token '+BeaconAPIService.readToken,
            'DeviceID': (window.device? window.device.uuid : "Browser"),
            'Content-Type': 'application/json'
          },
          data: JSON.stringify(eventsPayload)
        }))
        .then( function(response) {
            //Empty the log of events now that they have been successfully saved to the server.
            return BeaconAPIService.storage.removeItem('logEvents');
          }, function(err){
            //Shift the failed events back into the start of the buffer (before the new events).
            eventsPayload.events.forEach( function(failedEvent){ BeaconAPIService.logEvents.unshift( failedEvent ); });

            //Save the events into the storage so they persist between app restarts.
            return BeaconAPIService.storage.setItem( 'logEvents', BeaconAPIService.logEvents );
          });
      }
    },

    sendLoggedBeacons: function(){
      if( BeaconAPIService.logGetBeaconInfo.length >= 1 ){
        //Remove duplicates caused by a beacon being within several monitored regions.
        /* jshint camelcase:false */
        BeaconAPIService.logGetBeaconInfo = _.uniq(BeaconAPIService.logGetBeaconInfo, function(x){ return x.occurred_at+x.uuid+x.major+x.minor+x.rx; });
        var payload = {"deviceTime": Math.floor(new Date().getTime()/1000), "logs": BeaconAPIService.logGetBeaconInfo };
        BeaconAPIService.logGetBeaconInfo = []; //Create new array so any future events go into a new list to be sent next time.
        BeaconAPIService.storage.setItem('lastSentLoggedBeacons', new Date().getTime());
        log.info( "Logging beacons seen..."+ JSON.stringify(payload) );
        return Q(jQuery.ajax({
          method: 'POST',
          url: BeaconAPIService.blispaURL+"/logsbatch",
          headers: {
            'Authorization': 'Token '+BeaconAPIService.readToken,
            'DeviceID': (window.device? window.device.uuid : "Browser"),
            'Content-Type': 'application/json'
          },
          data: JSON.stringify(payload)
        }))
        .then( function() {
            //Empty the log of beacon events now that they have been successfully saved to the server.
            log.info('Beacons successfully batch-logged.');
            return BeaconAPIService.storage.removeItem('logGetBeaconInfo');
          }, function(err){
            //Shift the failed events back into the start of the buffer (before the new events).
            log.info('Beacons failed to be batch-logged.');
            payload.logs.forEach( function(failedLog){ BeaconAPIService.logGetBeaconInfo.unshift( failedLog ); });

            //Save the events into the storage so they persist between app restarts.
            return BeaconAPIService.storage.setItem( 'logGetBeaconInfo', BeaconAPIService.logGetBeaconInfo );
          });
      }
      else{
        log.info('No beacon logs to send.');
      }
    },

    //If there are events to record, send them regularly (to avoid turning the radio on quite so frequently).
    setLoggedEventsInterval: function(ms){
      if( BeaconAPIService.sendEventsInterval ){
        clearInterval( BeaconAPIService.sendEventsInterval );
      }
      BeaconAPIService.sendEventsInterval = setInterval( function(){
        BeaconAPIService.sendLoggedBeacons();
        BeaconAPIService.sendLoggedEvents();
      }, ms);

      BeaconAPIService.sendEventsIntervalTime = ms;

      //Store sendEventsInterval so that the Android background process sends at the same rate.
      document.addEventListener('deviceready', function() {
        if (window.plugins && window.plugins.appPreferences) {
          window.plugins.appPreferences.store('sendEventsInterval', BeaconAPIService.sendEventsIntervalTime);
        }
      });
    },

    initialize: function(){
      var deferred = Q.defer();

      //Use the appPreferences plugin when running on phone, or localStorage when running in Browser.
      BeaconAPIService.storage = {};
      if( window.plugins && window.plugins.appPreferences ){
        log.info('Using appPreferences for storage.');
        BeaconAPIService.storage.getItem = function(key){ return window.plugins.appPreferences.fetch(key); };
        BeaconAPIService.storage.setItem = function(key, val){ return window.plugins.appPreferences.store(key, val); };
        BeaconAPIService.storage.removeItem = function(key){ return window.plugins.appPreferences.remove(key); };
      }
      else{
        log.info('Using localStorage for storage.');
        BeaconAPIService.storage.getItem = function(key){ return Q.when(JSON.parse(localStorage.getItem(key))); };
        BeaconAPIService.storage.setItem = function(key, val){ return Q.when(localStorage.setItem(key, JSON.stringify(val))); };
        BeaconAPIService.storage.removeItem = function(key){ return Q.when(localStorage.removeItem(key)); };
      }

      //Send events when app goes into the background (in case it does not wake up again).
      document.addEventListener('pause', function(){
        BeaconAPIService.sendLoggedEvents();
        BeaconAPIService.sendLoggedBeacons();
        BeaconAPIService.storage.setItem('cachedBeacons', BeaconAPIService.cachedBeacons);
      }, false );

      //Send events when app is resumed (on iOS, it may only be awake for a few seconds- not long enough for the interval to fire)
      document.addEventListener('resume', function(){
        BeaconAPIService.sendLoggedEvents();
        BeaconAPIService.sendLoggedBeacons();
      }, false);

      //On startup, try to load any existing stored events from storage.
      var logEventsInit = BeaconAPIService.storage.getItem('logEvents').then( function(logEvents){
        if(!_.isEmpty(logEvents)) {
          BeaconAPIService.logEvents = logEvents;
        }
      });

      var logGetBeaconInfoInit = BeaconAPIService.storage.getItem('logGetBeaconInfo').then( function(logGetBeaconInfo){
        if(!_.isEmpty(logGetBeaconInfo)) {
          BeaconAPIService.logGetBeaconInfo = logGetBeaconInfo;
        }
      });

      var cachedBeaconsInit = BeaconAPIService.storage.getItem('cachedBeacons').then( function(cachedBeacons){
        if(!_.isEmpty(cachedBeacons)) {
          BeaconAPIService.cachedBeacons = cachedBeacons;
        }
      });


      BeaconAPIService.refreshBeaconCacheIfNeeded();

      //Only resolve promise and set as initialized once we've loaded from local caches (but this promise does NOT wait to load from remote cache - for that, you need to wait for cacheDeferred too).
      Q.all([logEventsInit, logGetBeaconInfoInit, cachedBeaconsInit])
        .then( function(){
          deferred.resolve();
        })
        .catch(function(err){
        console.error(err);
      });

      return deferred.promise;
    }

  };
  BeaconAPIService.setLoggedEventsInterval(5*60*1000); //Default. Sets the javascript SetInterval going. Will be overridden by future calls to setLoggedEventsInterval.

  window.BeaconAPIService = BeaconAPIService;
})();
