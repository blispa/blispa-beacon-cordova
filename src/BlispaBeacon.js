'use strict';
(function(){

  var log = window.log.getLogger('BlispaBeacon');

  // Custom event polyfill
  if(!window.CustomEvent) {
    (function() {
      var CustomEvent;

      CustomEvent = function(event, params) {
        var evt;
        params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined
          };
        evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
      };

      CustomEvent.prototype = window.Event.prototype;

      window.CustomEvent = CustomEvent;
    })();
  }

  var deferred = Q.defer();

  var BlispaBeacon = {

    debug: false,
    actionHistory: [],
    beaconsInRange: {},
    deferred: deferred,
    initialized: deferred.promise,
    autoEnableBluetooth: true,
    distances: {"ProximityImmediate": 0, "ProximityNear": 1, "ProximityFar": 2, "": 3, "ProximityUnknown": 3},

    ignoreRSSIBelow: 0,
    requiredOutOfRangeExitPings: 7, //Must go out of range for this many "ranges" (= this many seconds in foreground) before the "exit" will be completed (so exiting and stopping right on the edge doesn't re-trigger the feed item as it is found & hidden).
    requiredInRangeEnterPings: 1, //Beacon must appear constantly for this amount of "ranges"/"pings" without disappearing again for the action to trigger (so beacons that transiently appear once don't cause feed items to be displayed so long as this is set to higher than 1).

    //TODO: Add a method that returns beaconsInRange sorted by distance.

    initialize: function(){
      var BeaconService; //Holds the underlying class, which could be mocked or a CordovaBeaconService.
      if (window.cordova) {
        BeaconService = new CordovaBeaconService();
        BlispaBeacon.addDummyRegion = function(){ /* Do nothing if running on a real device */ };
        BlispaBeacon.addMonitoredBeaconRegion = BeaconService.addMonitoredBeaconRegion;
      } else {
        BeaconService = new MockBeaconService();
        BlispaBeacon.addDummyRegion = BeaconService.addDummyRegion;
        BlispaBeacon.addMonitoredBeaconRegion = function(){ /* Do nothing if running on a browser. The MockBeaconService emits dummy beacons regardless of whether it was meant to be monitoring a region containing them or not. */ };
      }

      BeaconService.initialize(BlispaBeacon.autoEnableBluetooth);
      BeaconService.startMonitoring();

      var APIInitPromise = BeaconAPIService.initialize();

      //Make methods on BeaconService callable by the external app.
      BlispaBeacon.setBackgroundBetweenScanPeriod = BeaconService.setBackgroundBetweenScanPeriod;
      BlispaBeacon.setBackgroundScanPeriod = BeaconService.setBackgroundScanPeriod;

      //If there is a saved actionHistory, load it.
      var savedActionHistory = localStorage.getItem('BlispaActionHistory');
      if( savedActionHistory ){
        BlispaBeacon.actionHistory = JSON.parse(savedActionHistory);
        //actionHistory elements contain a .time, when they were seen. Since we don't store duplicate actions (so we have no single 'time'), recreate it and use lastSeen as the time we saw the action.
        //They do not contain 'firstSeen' and 'lastSeen' so remove those.
        BlispaBeacon.actionHistory.forEach( function(action){
          action.time = new Date( action.lastSeen );
          //Create another action in actionHistory with time set to firstSeen as there must have been an observation of this action at that point (there may also have been observations in between but we don't store them between app restarts).
          if(action.firstSeen){
           var firstSeenAction = _.clone(action);
           firstSeenAction.time = new Date( action.firstSeen );
           delete firstSeenAction.firstSeen;
           delete firstSeenAction.lastSeen;
           BlispaBeacon.actionHistory.push(firstSeenAction);
           }
          delete action.firstSeen;
          delete action.lastSeen;
        });
      }

      document.addEventListener('beaconStartToMove', function(event){
        var moveDetails = event.detail;
        log.info('beaconStartToMove received: '+moveDetails.beacon.id+' '+moveDetails.direction+' from '+moveDetails.oldProximity+' to '+moveDetails.newProximity+'!');
        //Start doing the request to the API to add data to the beacon (before it's actually confirmed the beacon has appeared, for speed).
        var apiPromise = BeaconAPIService.getBeaconInfo(moveDetails.beacon.id, true, moveDetails.oldProximity, moveDetails.newProximity);
        //When the beacon entering timeout completes (movePromise resolves), add the item to the history list.
        moveDetails.movePromise.then( function(){
          var nowInRange = false;
          log.info('Move completed.');
          if(moveDetails.direction === "approaching" && (moveDetails.newProximity === "ProximityFar" || moveDetails.newProximity === "ProximityNear" || moveDetails.newProximity === "ProximityImmediate")){
            BlispaBeacon.beaconsInRange[moveDetails.beacon.id] = moveDetails.beacon;
            BlispaBeacon.beaconsInRange[moveDetails.beacon.id].actions = []; //will be overridden below if we get actions returned from server.
            nowInRange = true;
          }
          else if(moveDetails.direction === "retreating" && (moveDetails.newProximity === "" || moveDetails.newProximity === "ProximityUnknown")){
            delete BlispaBeacon.beaconsInRange[moveDetails.beacon.id];
          }
          apiPromise.then( function(apiResponse){
            log.info('Move completed and API response recieved.');
            if( nowInRange ) {
              //Add actions onto beaconsInRange.
              BlispaBeacon.beaconsInRange[moveDetails.beacon.id] = apiResponse;
            }

            //TODO: Change meta to "approachingActions" and "retreatingActions" in API? Also "approachingFar" / "approachingNear"?
            // Or should the API just send a list of things that are tied to this point and the app determines how to respond, which
            // would mean either adding direction/proximity to the action in the code below (so the app can determine if the action is relevant)
            // - would need better noDupes logic as an action that is approaching & the same action retreating both need to be saved.
            // - Downside: The action is no longer really an action, but a "potential action" that might be acted on by the app.
            // Or some kind of subscription mechanism where an app can tell this library that it is interested in
            // (eg.) actions of Type X, when they are approaching and closer than 'Near', but not when retreating.
            if( nowInRange && apiResponse.actions && Array.isArray( apiResponse.actions ) ){
              apiResponse.actions.forEach( function(action){
                BlispaBeacon.fireAction(action, moveDetails.oldProximity, moveDetails.newProximity);
              });
              BlispaBeacon.saveActionHistory();
            }
            document.dispatchEvent( new CustomEvent("beaconMoved", {detail: {oldProximity: moveDetails.oldProximity, newProximity: moveDetails.newProximity, direction: moveDetails.direction, beacon: apiResponse}}));
          }, function(err){
            //apiResponse failed. Still dispatch the beaconMoved event, but without any actions (eg. so that child-finder can trigger an in/out of range).
            log.info("apiResponse Failed (but movePromise succeeded). "+JSON.stringify(err));
            document.dispatchEvent( new CustomEvent("beaconMoved", {detail: {oldProximity: moveDetails.oldProximity, newProximity: moveDetails.newProximity, direction: moveDetails.direction, beacon: moveDetails.beacon }}));
          });
        }, function(err){
          //movePromise failed (eg. the beacon came back into range). it's no big deal. Fail silently.
          log.info("movePromise Failed. "+JSON.stringify(err));
        })
        .catch( function(err){ //Catch when errors occur within the sending of events.
          console.error(err);
        }) ;
      });

      //Resolve the promise so that things outside this library can start using it once all the components are initialized.
      Q.all([APIInitPromise]).then(function() {
        BlispaBeacon.deferred.resolve();
      });

      return BlispaBeacon.deferred.promise;
    },

    calculateDirection: function(oldProximity, newProximity){
      var direction = "";
      if( BlispaBeacon.distances[newProximity] <= BlispaBeacon.distances[oldProximity] ){
        direction = "approaching";
      }
      else{
        direction = "retreating";
      }
      return direction;
    },

    //Save the new ActionHistory so that it persists across app restarts. Only save the most recently seen copy of each action.
    saveActionHistory: function(){
      var actionHistoryNoDupes = BlispaBeacon.getActionHistoryNoDupes().map(function(x){ delete x.time; return x; });
      localStorage.setItem('BlispaActionHistory', JSON.stringify( actionHistoryNoDupes ));
    },

    /** Turns on/off debug logging from both here, and the background plugin.*/
    setDebug: function (debug){
      BlispaBeacon.debug = debug;
      if( debug ) {
        log.setLevel("DEBUG");
      }
      else{
        log.setLevel("WARN");
      }
      //Save to preferences (if they exist) so that the background service also gets this debug setting on restart.
      if( window.plugins && window.plugins.appPreferences ){
        return window.plugins.appPreferences.store('debug', debug);
      }
    },

    /**
     * Adds the beacon to actionHistory, and emits the beaconAction event.
     * @param action
     * @param oldProximity
     * @param newProximity
     */
    fireAction: function(action, oldProximity, newProximity){
      action.time = new Date();
      //Add firstSeen & lastSeen onto the object that gets returned (eg. so we can determine if it's been a long time if we last saw this action and send a notification).
      var actionFromHistory = _.find(BlispaBeacon.getActionHistoryNoDupes(), {id: action.id} );
      var lastSeen;
      var firstSeen;
      var direction = BlispaBeacon.calculateDirection(oldProximity, newProximity);
      if( actionFromHistory ) {
        lastSeen = actionFromHistory.lastSeen;
        firstSeen = actionFromHistory.firstSeen;
      }
      BlispaBeacon.actionHistory.push( action ); //Now we've got the last time it was seen, add the new sighting.
      document.dispatchEvent(new CustomEvent("beaconAction", {detail: {oldProximity: oldProximity, newProximity: newProximity, direction: direction, actionFirstSeen: firstSeen, actionLastSeen: lastSeen, action: action}}));
    },

    clear: function(){
      BlispaBeacon.actionHistory.length = 0;
      //Re-add those beacons that are currently in-range.
      _.values(BlispaBeacon.beaconsInRange).forEach( function(beacon){
        if( beacon.actions && Array.isArray( beacon.actions ) ){
          beacon.actions.forEach( function(action){
            action.time = new Date();
            BlispaBeacon.actionHistory.push( action );
            log.info("Re-adding action after clearing actionHistory");
          });
        }
      });
      //Overwrite saved actions with the new (cleared) ones.
      BlispaBeacon.saveActionHistory();
    },

    getNearBeacons: function(){
      return _.values(BlispaBeacon.beaconsInRange);
    },
    getNearBeaconsKeyedById: function(){
      return BlispaBeacon.beaconsInRange;
    },

    fireAllActions: function(){
      _.values(BeaconAPIService.getCachedActionsOfType("all")).forEach( function(action) {
        BlispaBeacon.fireAction(action, "ProximityUnknown", "ProximityImmediate");
      });
    },

    //Returns the list of actions that have been triggered, with each action only appearing once, with firstSeen & lastSeen properties.
    // Oldest action will be first in the list.
    getActionHistoryNoDupes: function(){
      var actionHistoryNoDupes = {};
      for( var i=0; i < BlispaBeacon.actionHistory.length; i++ ){
        var action = _.clone(BlispaBeacon.actionHistory[i]); //clone so we don't add firstSeen / lastSeen to the original action, and can delete 'time' from the copy.
        if( !actionHistoryNoDupes[action.id] ){
          action.firstSeen = action.time.getTime(); //Must use getTime() since dates get serialized to strings when saved in localStorage and it's easier to deserialize ints.
          action.lastSeen = action.time.getTime();
          delete action.time;
          actionHistoryNoDupes[action.id] = action;
        }
        else{ //Previously seen
          //If we've seen it more recently, modify the lastSeen time.
          if( action.time.getTime() > parseInt(actionHistoryNoDupes[action.id].lastSeen) ){
            actionHistoryNoDupes[action.id].lastSeen = action.time.getTime();
          }
          if( action.time.getTime() < parseInt(actionHistoryNoDupes[action.id].firstSeen) ){
            actionHistoryNoDupes[action.id].firstSeen = action.time.getTime();
          }
        }
      }

      return _.sortBy(_.values(actionHistoryNoDupes), 'lastSeen');
    },

    //Expose other methods
    getAction: BeaconAPIService.getAction,
    getApps: BeaconAPIService.getApps,
    refreshBeaconCache: BeaconAPIService.refreshBeaconCache,
    getAllCachedBeacons: BeaconAPIService.getAllCachedBeacons,
    getCachedBeaconCount: BeaconAPIService.getCachedBeaconCount,
    getCachedActionsOfType: BeaconAPIService.getCachedActionsOfType,
    setAPIReadToken: BeaconAPIService.setReadToken,
    getAPIReadToken: function(){ return BeaconAPIService.readToken; },
    setAPIURL: BeaconAPIService.setAPIURL,
    setWPAPIURL: BeaconAPIService.setWPAPIURL,
    getAPIURL: function(){ return BeaconAPIService.blispaURL; },
    getWPAPIURL: function(){ return BeaconAPIService.wpURL; },
    recordEvent: BeaconAPIService.recordEvent,
    registerDevice: BeaconAPIService.registerDevice,
    setLoggedEventsInterval: BeaconAPIService.setLoggedEventsInterval,
  };

  //As soon as cordova plugins have finished loading, start listening for beacons.
  if (document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1) {
    document.addEventListener("deviceready", function(){
      BlispaBeacon.initialize();
    });
  } else {
    //this is the browser. Wait until loaded to give other scripts a chance to modify autoEnableBluetooth.
    window.addEventListener("load", function() {
      BlispaBeacon.initialize();
    });
  }

  window.BlispaBeacon = BlispaBeacon;
})();
