'use strict';
window.CordovaBeaconService = function(){

  var log = window.log.getLogger('BlispaBeacon');

  function beaconWithinRegion(region, beacon){
    if( region.minor ){
      return region.uuid.toLowerCase() === beacon.uuid.toLowerCase() && region.major === beacon.major && region.minor === beacon.minor;
    }
    else if( region.major ){
      return region.uuid.toLowerCase() === beacon.uuid.toLowerCase() && region.major === beacon.major;
    }
    else if( region.uuid){
      return region.uuid.toLowerCase() === beacon.uuid.toLowerCase();
    }
    else{ //Either region has no uuid / major / minor set, or it's not a region.
      return true;
    }
  }

  function broadcastBeaconStartToMove( oldProximity, newProximity, beacon, movePromise ){
    var direction = BlispaBeacon.calculateDirection(oldProximity, newProximity);
    var myEvent = new CustomEvent("beaconStartToMove", {detail: {oldProximity: oldProximity, newProximity: newProximity, direction: direction, beacon: beacon, movePromise: movePromise}});
    document.dispatchEvent(myEvent);
  }

  /**Adds this region to the background service so we continue to monitor for it after the app dies.
    Debounced so that if addMonitoredBeaconRegion is called many times in quick succession (eg. on app start) we only save the final list of regions.
   **/
  var debouncedAddRegionsToSharedPreferences = _.debounce( function(regions) {
    var beaconsAsStrings = regions.map( function(region){ return region.uuid+"/"+(region.major?region.major:"")+"/"+(region.minor?region.minor:""); });
    log.info('Saving beaconRegions to SharedPreferences: '+beaconsAsStrings);
    return window.plugins.appPreferences.store('beaconRegions', beaconsAsStrings);
  }, 100);

  var BeaconService = {
    allBeacons: {},
    monitoringBeacons: [],
    enteringEventPromises: {},
    exitingEventPromises: {},

    initialize: function(autoEnableBluetooth){
      log.info('initializing!');
      if( typeof(window.cordova) === 'undefined' ){
        throw "Can't find 'window.cordova. You should use MockBeaconService instead of this class (CordovaBeaconService) when not testing on a real device.'";
      }
      if (typeof(cordova.plugins.locationManager) === 'undefined') {
        throw "Can't find 'cordova.plugins.locationManager' in this context";
      }
      if (!window.plugins || typeof(window.plugins.appPreferences) === 'undefined') {
        throw "Can't find 'window.plugins.appPreferences' in this context";
      }

      //cordova.plugins.locationManager.disablelog.infos();
      //cordova.plugins.locationManager.enablelog.infos();

      //setup events you want to listen to
      var delegate = new cordova.plugins.locationManager.Delegate();

      //Enable bluetooth (only works on Android).
      if(autoEnableBluetooth ) {
        log.info('autoEnablingBluetooth!');
        cordova.plugins.locationManager.isBluetoothEnabled()
          .then(function (isEnabled) {
            log.info("bluetooth enabled: " + isEnabled);
            if (!isEnabled) {
              cordova.plugins.locationManager.enableBluetooth();
            }
          })
          .fail(console.error)//TODO: Test and handle (display a message) on iOS if bluetooth is off.
          .done();
      }
      else{
        log.info('autoEnableBluetooth is false so not enabling it.');
      }


      delegate.didEnterRegion = function(result) {
        log.info('Entered Monitored Region');
        log.info( JSON.stringify( result ) );
        //BeaconService.startRanging( result.region );
      };
      delegate.didExitRegion = function(result) {
        log.info('Exited Region');
        //BeaconService.stopRanging( result.region );
      };
      delegate.didDetermineStateForRegion = function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('didDetermineStateForRegion: '+ JSON.stringify(result));
        log.info('didDetermineStateForRegion: '+ JSON.stringify(result));
      };

      delegate.didStartMonitoringForRegion = function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('didStartMonitoringForRegion: '+ JSON.stringify(result.region));
      };
      delegate.monitoringDidFailForRegionWithError = function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('monitoringDidFailForRegionWithError: '+ JSON.stringify(result.region));
      };
      delegate.didRangeBeaconsInRegion = function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('didRangeBeaconsInRegion: '+ JSON.stringify(result));
        log.debug( new Date()+':didRangeBeaconsInRegion '+ JSON.stringify(result));

        var beaconsFoundThisPass = {};
        log.debug( result.beacons.length+' beacons ranged');
        result.beacons.forEach( function(beacon){
          if( beacon.proximity === 'ProximityUnknown' || ( !!BlispaBeacon.ignoreRSSIBelow && beacon.rssi < BlispaBeacon.ignoreRSSIBelow) ){
            //ignore ProximityUnknown beacons. Treat them as though they weren't seen.
            return;
          }
          var beaconId = (beacon.uuid+"/"+beacon.major+"/"+beacon.minor).toLowerCase();
          beacon.id = beaconId;
          beacon.seen = new Date();
          beaconsFoundThisPass[beaconId] = 1;

          //If it's a brand new beacon, create it, empty, in allBeacons.
          if( !BeaconService.allBeacons[beaconId] ){
            BeaconService.allBeacons[beaconId] = {};
          }
          var beaconPreviously = _.clone(BeaconService.allBeacons[beaconId]); //Clone because we're about to update the values in place and need to store the old values.
          //Update the beacon's values in-place.
          _.assign( BeaconService.allBeacons[beaconId], beacon );

          //Log that we have seen this beacon.
          BeaconAPIService.addToBatchLogs(beacon);

          //TODO: Write Tests.
          //If beacon has entered since last time we saw it, broadcast that move.
          if( !beaconPreviously.proximity ){
            // Brand new beacon or beacon that was previously not in range. Cancel any "exit" triggers that were about to fire or queue an "enter" trigger.
            if( BeaconService.exitingEventPromises[beaconId] ){
              log.info('Cancelling exit for '+beaconId+'...');
              delete BeaconService.exitingEventPromises[beaconId];

              //If the beacon reappeared at a different distance to before it started exiting, broadcast that as a move.
              //Otherwise, a single missed frame right before moving from Near to Immediate (for example) will start & then cancel an exit, without broadcasting the move Near->Immediate.
              if( BlispaBeacon.distances[beacon.proximity] !== BlispaBeacon.distances[beaconPreviously.exitFromProximity] ){
                log.info('Beacon reappeared before exiting at a different distance to when it went away. Immediately broadcasting move from '+beaconPreviously.exitFromProximity+" to "+beacon.proximity);
                broadcastBeaconStartToMove(beaconPreviously.exitFromProximity, beacon.proximity, BeaconService.allBeacons[beaconId], Q.resolve() );
              }
              delete beacon.exitFromProximity;
            }
            else{ //Only trigger a re-enter if there was not already an exit scheduled. Otherwise a single missed packet will start & cancel an exit, and then trigger a new enter event in 3 secs.
              BeaconService.enteringEventPromises[beaconId] = {
                deferred: Q.defer(),
                consecutivePingsSeen: 0,
                incrementPingsSeen: _.debounce(function() { //Must use a debounce function or beacons in multiple regions will increment consecutivePingsSeen many times for each detection (once for each region they are in)
                  BeaconService.enteringEventPromises[beaconId].consecutivePingsSeen++;
                  log.info(beaconId + ' has now seen ' + BeaconService.enteringEventPromises[beaconId].consecutivePingsSeen + ' pings.');

                  //If we have seen enough pings, actually fire the enter by resolving the "enter" promise.
                  if (BeaconService.enteringEventPromises[beaconId].consecutivePingsSeen >= BlispaBeacon.requiredInRangeEnterPings) {
                    BeaconService.enteringEventPromises[beaconId].deferred.resolve();
                    log.info(beaconId + ' entered after seeing '+BeaconService.enteringEventPromises[beaconId].consecutivePingsSeen+' pings');
                    delete BeaconService.enteringEventPromises[beaconId];
                  }
                }, 500, {leading: true, trailing: false}) //Ignore "duplicates" for less time than the interval between completing ranges.
              };

              log.info('Queueing enter event to happen after we see '+BlispaBeacon.requiredInRangeEnterPings+' pings for '+beaconId+'...');
              broadcastBeaconStartToMove("", beacon.proximity, BeaconService.allBeacons[beaconId], BeaconService.enteringEventPromises[beaconId].deferred.promise);
            }
          }
          else if( beaconPreviously.proximity !== beacon.proximity ){
            //Beacon has changed proximity class, but not in/out of range. It was already previously known about. No need to wait as the moving-average already smooths out false enter/exits.
            log.info("Beacon moved between proximity classes. Broadcasting immediately: "+beaconPreviously.proximity+" to "+beacon.proximity);
            broadcastBeaconStartToMove(beaconPreviously.proximity, beacon.proximity, BeaconService.allBeacons[beaconId], Q.resolve() );
          }

          if( BeaconService.enteringEventPromises[beaconId] ){
            BeaconService.enteringEventPromises[beaconId].incrementPingsSeen();
          }
        });

        //Check through our internal lists of beacons and remove anything that is no longer visible (set to out of range).
        _.values(BeaconService.allBeacons).forEach( function(formerBeacon){
          //if this beacon used to be close by and wasn't this time, and this is a relevant region that SHOULD have detected it...
          if( !beaconsFoundThisPass[formerBeacon.id] && beaconWithinRegion(result.region, formerBeacon) ) {
            //If beacon used to be in range, then start the exit process:
            if (formerBeacon.proximity) {
              var oldProximity = formerBeacon.proximity;
              formerBeacon.proximity = "";
              // Beacon that has just gone out of range. Cancel any "enter" triggers that were about to fire (if any), and queue an "exit" trigger.
              if (BeaconService.enteringEventPromises[formerBeacon.id]) {
                log.info('Cancelling entering for ' + formerBeacon.id + '...');
                delete BeaconService.enteringEventPromises[formerBeacon.id];
              }
              else {
                log.info('Queueing exit event to happen in ' + BlispaBeacon.requiredOutOfRangeExitPings + ' missed pings time for ' + formerBeacon.id + '...');

                BeaconService.exitingEventPromises[formerBeacon.id] = {
                  deferred: Q.defer(),
                  consecutivePingsMissed: 0,
                  incrementPingsMissed: _.debounce(function() { //Must use a debounce function or beacons in multiple regions will increment consecutivePingsMissed many times for each detection (once for each region they are in)
                    BeaconService.exitingEventPromises[formerBeacon.id].consecutivePingsMissed++;
                    log.info(formerBeacon.id + ' has now missed ' + BeaconService.exitingEventPromises[formerBeacon.id].consecutivePingsMissed + ' pings.');

                    //If we have missed enough pings, actually fire the exit by resolving the "exit" promise.
                    if (BeaconService.exitingEventPromises[formerBeacon.id].consecutivePingsMissed >= BlispaBeacon.requiredOutOfRangeExitPings) {
                      BeaconService.exitingEventPromises[formerBeacon.id].deferred.resolve();
                      log.info(formerBeacon.id + ' exited after missing '+BeaconService.exitingEventPromises[formerBeacon.id].consecutivePingsMissed+' pings');
                      delete BeaconService.exitingEventPromises[formerBeacon.id];
                    }
                  }, 500, {leading: true, trailing: false}) //Ignore "duplicates" for less time than the interval between completing ranges.
                };
                formerBeacon.exitFromProximity = oldProximity;
                broadcastBeaconStartToMove(oldProximity, "", formerBeacon, BeaconService.exitingEventPromises[formerBeacon.id].deferred.promise);
              }
            }

            if (BeaconService.exitingEventPromises[formerBeacon.id]) {
              //Beacon is on the way out. Increment the number of pings missed.
              BeaconService.exitingEventPromises[formerBeacon.id].incrementPingsMissed();
            }
          }
        });

        /*console.log(_.size(beaconsFoundThisPass) + " visible beacons ("+
         _.size(BeaconService.allBeacons)+" ever seen, currently "+
         _.values(BeaconService.allBeacons).filter( function(x){ return x.proximity === "ProximityImmediate"; }).length+" immediate, "+
         _.values(BeaconService.allBeacons).filter( function(x){ return x.proximity === "ProximityNear"; }).length+" near, "+
         _.values(BeaconService.allBeacons).filter( function(x){ return x.proximity === "ProximityFar"; }).length+" far, "+
         _.values(BeaconService.allBeacons).filter( function(x){ return x.proximity == ""; }).length+" out of range)");
         */

      };
      delegate.rangingBeaconsDidFailForRegion= function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('rangingBeaconsDidFailForRegion: '+ JSON.stringify(result.region));
        console.error('rangingBeaconsDidFailForRegion: '+ JSON.stringify(result));
      };

      delegate.didChangeAuthorizationStatus= function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('didChangeAuthorizationStatus: '+ JSON.stringify(result.region));
      };

      delegate.peripheralManagerDidStartAdvertising= function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('peripheralManagerDidStartAdvertising: '+ JSON.stringify(result.region));
      };
      delegate.peripheralManagerDidUpdateState= function(result) {
        //cordova.plugins.locationManager.appendToDeviceLog('peripheralManagerDidUpdateState: '+ result.state);
      };

      cordova.plugins.locationManager.setDelegate(delegate);

      cordova.plugins.locationManager.requestAlwaysAuthorization();

      //Fixes bug where beacon monitors/ranges don't make it through to javascript on first launch / after app has been force stopped.
      // (I guess the call to _registerDelegateCallbackId which happens as part of the cordova-plugin initialization happens too soon / doesn't get saved?)
      cordova.plugins.locationManager._registerDelegateCallbackId();

      //Set up a monitoring region for ALL Blispa beacons.
      BeaconService.monitoringBeacons = [BeaconService.createBeaconRegion("Blispa", "02424C49-5350-4F00-9DBF-3F5307B1159A")];
      log.info('intialized');
    },

    createBeaconRegion: function (identifier, uuid, major, minor) {
      log.info('createBeaconRegion('+identifier+','+ uuid+','+ major+','+ minor);

      var beacon = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor, true);
      log.info( JSON.stringify(beacon) );
      return beacon;
    },

    addMonitoredBeaconRegion: function(identifier, uuid, major, minor){
      var region = BeaconService.createBeaconRegion(identifier, uuid, major, minor);
      BeaconService.monitoringBeacons.push(region);
      if( BeaconService.isMonitoring ){
        cordova.plugins.locationManager.startMonitoringForRegion(region);
        cordova.plugins.locationManager.requestStateForRegion(region);
        //We initally range ALL Blispa UUID beacons, don't need to range regions individually. Only monitor.
        if( uuid.toLowerCase() !== "02424c49-5350-4f00-9dbf-3f5307b1159a" ){
          log.info('Starting Ranging for a non-Blispa-UUID beacon.');
          cordova.plugins.locationManager.startRangingBeaconsInRegion(region);
        }
      }

      log.info("Adding region to monitored regions.");
      return debouncedAddRegionsToSharedPreferences(BeaconService.monitoringBeacons);
    },

    startMonitoring: function(){
      BeaconService.isMonitoring = true;

      if (!cordova.plugins.locationManager) {
        console.error("'cordova.plugins.locationManager' missing for Start monitoring");
      } else if (BeaconService.monitoringBeacons && BeaconService.monitoringBeacons.length>0) {
        for (var i = 0; i < BeaconService.monitoringBeacons.length; i++) {
          var beacon = BeaconService.monitoringBeacons[i];
          cordova.plugins.locationManager.startMonitoringForRegion(beacon);
          cordova.plugins.locationManager.requestStateForRegion(beacon);
          cordova.plugins.locationManager.startRangingBeaconsInRegion(beacon);
        }
        log.info("Start monitoring");

      } else {
        log.info("No beacons to start monitoring");
      }
    },

    startRanging: function(region){
      if (!cordova.plugins.locationManager) {
        console.error("'cordova.plugins.locationManager' missing for Start monitoring");
      } else{
        if( region.constructor.name !== 'BeaconRegion' ){
          //Take the passed in message and hydrate back to a REAL region (not some deserialized JSON from the native Java).
          region = _.find(BeaconService.monitoringBeacons, {identifier: region.identifier} );
        }

        if( region.constructor.name === 'BeaconRegion' ){
          cordova.plugins.locationManager.startRangingBeaconsInRegion(region);
          log.info( 'starting ranging' );
        }
        else{
          console.error('Entered a region we are not monitoring (perhaps a native android region?). Don\'t have a region to start ranging for.');
        }
      }
    },

    stopRanging: function(region){
      if (!cordova.plugins.locationManager) {
        console.error("'cordova.plugins.locationManager' missing for Stop monitoring");
      } else {
        if( region.constructor.name !== 'BeaconRegion' ){
          //Take the passed in message and hydrate back to a REAL region (not some deserialized JSON from the native Java).
          region = _.find(BeaconService.monitoringBeacons, {identifier: region.identifier} );
        }

        if( region.constructor.name === 'BeaconRegion' ){
          cordova.plugins.locationManager.stopRangingBeaconsInRegion(region);
          log.info( 'stopping ranging' );
        }
        else{
          console.error('Exited a region we are not monitoring (perhaps a native android region?). Don\'t have a region to start ranging for.');
        }
      }
    },

    setBackgroundBetweenScanPeriod: function(intervalMs){
      return window.plugins.appPreferences.store('backgroundBetweenScanPeriod', intervalMs);
    },

    setBackgroundScanPeriod: function(intervalMs){
      return window.plugins.appPreferences.store('backgroundScanPeriod', intervalMs);
    }

  };

  return BeaconService;
};