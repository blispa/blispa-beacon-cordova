'use strict';
window.MockBeaconService = function(){

  var BeaconService = {
    visibleBeacons: [],
    monitoringBeacons: [],
    intialized: false,

    initialize: function(autoEnableBluetooth){
      BeaconService.addDummyRegion('02424C49-5350-4F00-9DBF-3F5307B1159A',16,1);
      //BeaconService.addTestRegions();
      //console.log('MockBeaconService initialized with autoEnableBluetooth='+autoEnableBluetooth);
      BeaconService.intialized = true;
    },

    createBeaconRegion: function (identifier, uuid, major, minor) {
      //console.log('createBeaconRegion('+identifier+','+ uuid+','+ major+','+ minor);
      //Simulated cordova region.
      var beacon = {"typeName":"BeaconRegion","uuid":uuid,"major":major,"minor":minor,"identifier":identifier};

      return beacon;
    },

    addDummyRegion: function(uuid, major, minor){
      var dummyBeacon = BeaconService.createBeaconRegion('demoBeacon'+BeaconService.monitoringBeacons.length,uuid, major, minor);
      BeaconService.monitoringBeacons.push( dummyBeacon );
    },

    startMonitoring: function(){
      BeaconService.isMonitoring = true;

      if (BeaconService.monitoringBeacons && BeaconService.monitoringBeacons.length>0) {
        //"Enter" and then "Exit" the first monitored beacon every few seconds.
        var beaconIdx = 0;
        var passNearBeacon = function(){
          var beaconToAdd = _.cloneDeep(BeaconService.monitoringBeacons[beaconIdx]);  //Clone in order to replicate the real CordovaBeaconService which returns a different object each time it enters the same region.
          var beaconId = beaconToAdd.uuid+"/"+beaconToAdd.major+"/"+beaconToAdd.minor;
          beaconToAdd.id = beaconId;

          BeaconService.visibleBeacons.push( beaconToAdd );
          //console.log('Entered Region');

          //Do broadcasts, using either response from the API or the empty undefined from above.
          var deferred = Q.defer();
          var movePromise = deferred.promise;
          movePromise.timeout = setTimeout( function(){
            deferred.resolve();

            //Log an observation of this beacon (simulates cordovaBeaconService, which also also does this).
            _.extend(beaconToAdd, {rssi: -85, tx: -59, accuracy: 4.53});
            BeaconAPIService.addToBatchLogs(beaconToAdd);
          }, 1000);
          var myEvent = new CustomEvent("beaconStartToMove", {detail: {oldProximity: "ProximityUnknown", direction: "approaching", newProximity: "ProximityNear", beacon: beaconToAdd, movePromise: movePromise}});
          document.dispatchEvent(myEvent);

          //After a few secs, "Exit" the beacon.
          setTimeout( function(){
            _.remove(BeaconService.visibleBeacons, function(x){ return x; }, beaconToAdd );
            //console.log('Exited Region');
            var deferred = Q.defer();
            var movePromise = deferred.promise;
            movePromise.timeout = setTimeout( function(){
              deferred.resolve();
            }, 1000);
            var myEvent = new CustomEvent("beaconStartToMove", {detail: {oldProximity: "ProximityNear", direction: "retreating", newProximity: "ProximityUnknown", beacon: beaconToAdd, movePromise: movePromise}});
            document.dispatchEvent(myEvent);
          }, 7000);

          //Set up that we will next "pass near" the next beacon (or reset back to the start of the list).
          beaconIdx++;
          beaconIdx = beaconIdx%BeaconService.monitoringBeacons.length;
        };

        setTimeout( passNearBeacon, 1000); //Initially enter a beacon after 1 second.
        setInterval( passNearBeacon, 10000); //Add and remove a beacon every 10 secs after that.
        //console.log("Started monitoring");

      }
    },

    //Dummy functions that do stuff in the real BeaconService, but not in the browser.
    setBackgroundBetweenScanPeriod: function(intervalMs){
      return Q.when(1);
    },
    setBackgroundScanPeriod: function(intervalMs){
      return Q.when(1);
    }
  };

  return BeaconService;
};